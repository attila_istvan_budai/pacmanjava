
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author ATL
 *
 */
public class MainClass {

	static int MAX_DEPTH = 10;
	

	static GameState previousGameState;
	
	static Scanner scanner = new Scanner(System.in);
	
	//static Map<PointPair, Integer> distanceMap = new HashMap<>();
	
	static boolean baseLogging  = true;
	static boolean detailedLogging  = false;
	
	static List<Point> pacmanPositionHistory = new ArrayList<>();
	
	static int myPacmanIndex = 0;
	static int currentRound = 0;
	static int currentDepth = 0;
	
	
	static boolean printFirstSuccessors = true;
	static boolean ghostMovementPrint = false;
	static int roundToPrint = 48;
	static int ghostDepthToPrint = 0;
	
	
	

	public static void main(String[] args) {

		while (true) {
			
			ElapsedTimer t = new ElapsedTimer();
			t.start();
			try {
				Reader read = new Reader(scanner);
				GameState state = read.getGameState();
				if(baseLogging) {System.err.println("############################ - Starting turn "+ state.tick+"  ####################");}
				currentRound = state.tick;
				myPacmanIndex = state.myPacmanIndex;

				if (read.data[2] == -1)
					break;
				
				doActionForThisTurn(state, read.data);


			} catch (Exception e) {
				e.printStackTrace();
			}

			if(baseLogging) {System.err.println("AI update took: " + t.getElapsedTime() + "ms");}
			
		}
	}
	
	
	public static void doActionForThisTurn(GameState state, int[] data){

		currentDepth = 0;
		Direction[] res = getBestAction(state);
		writeStepsToOutput(data, res);
		previousGameState = state;
	}

	private static void writeStepsToOutput(int[] data, Direction[] res) {
		Direction dir = res[0];
		if(res.length==2){
			Direction dir2 = res[1];
			System.out.println(String.format("%d %d %d %c %c", data[0], data[1], data[2], dir.dir, dir2.dir));
		} else{
			System.out.println(String.format("%d %d %d %c", data[0], data[1], data[2], dir.dir));
		}
	}

	public static int manhattanDistance(int x0, int y0, int x1, int y1) {
//		Point p1 = new Point(x0, y0);
//		Point p2 = new Point(x1, y1);
//		
//		PointPair pair = new PointPair(p1, p2);
//		if(distanceMap.containsKey(pair)){
//			int dist = distanceMap.get(pair);
//			System.err.println("Distance from cache " + p1 + " " + p2 + " -> " + dist);
//			return distanceMap.get(pair);
//		}
		
		return Math.abs(x1 - x0) + Math.abs(y1 - y0);
		
	}

	public static Direction[] getBestAction(GameState state) {
		List<Point> previousPositions = new ArrayList<>();
		previousPositions.add(new Point(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2]));
		pacmanPositionHistory.add(new Point(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2]));
		
		List<Direction> possibleActions = Action.getPossibleAction(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2], state.pacmanwalls,null);
		
		List<Direction> originalActions = new ArrayList<>(possibleActions);
		int[][] successors = getAllPossiblePositionsFromPoint(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2], possibleActions, state.walls.length, state.walls[0].length);

//		System.err.println("successorts " + Arrays.deepToString(successors));
		GameState[] states = generateGameStatesBasedOnMyMove(successors, state, 0);
		changeStatesByWorstPossibleGhostMoves(states, 0);
//		System.err.println("states " + states.length);
		
		StateWithDirections stateWithDirections = null;
		int maxDepth = MAX_DEPTH;
		if(isMyPacmanFast(state)){
			stateWithDirections = getTwoPossibleGameStatesWithPossibleActions(state, possibleActions);
			states = stateWithDirections.states;
			
		}
		
		//changeStateByGhostMove(states, 0);
		
		logFirstPositionsWithScore(states);
		logFirstGhostStates(states);

		int[] finalScores = new int[states.length];
		
		for(int i=0; i<states.length; i++){
			states[i].source = i;
		}
		
		Arrays.stream(states).forEach(s -> findMaxScore(s, maxDepth, finalScores));
		
		if(baseLogging) {System.err.println("possibleActions " + originalActions);}
		if(baseLogging) {System.err.println("finalScores " + Arrays.toString(finalScores));}
		
		int maxIndex  = IntStream.range(0, finalScores.length).reduce((a,b)->finalScores[a]<finalScores[b]? b: a).getAsInt();

		if(stateWithDirections!=null){
			DirectionWithParent dir = getDirectionsForTwoSteps(stateWithDirections, maxIndex);
			Direction[] res = new Direction[]{dir.parent, dir.dir};
			return res;
			
		}
		Direction[] res = new Direction[]{originalActions.get(maxIndex)};
		return res;

	}


	private static void logFirstGhostStates(GameState[] states) {
		if (detailedLogging) {
			for (GameState s : states) {
				System.err.println("Initial ghost states " + Arrays.deepToString(s.ghostStates));
			}
		}
	}


	private static void logFirstPositionsWithScore(GameState[] states) {
		if(printFirstSuccessors && roundToPrint == currentRound){
			System.err.println("First successors");
			for(int i=0; i<states.length; i++){
				System.err.println(Arrays.toString(states[i].pacmanStates[myPacmanIndex]) + " " + states[i].score);
				
			}	
		}
	}

	private static DirectionWithParent getDirectionsForTwoSteps(StateWithDirections stateWithDirections, int maxIndex) {
		DirectionWithParent dir = stateWithDirections.possibleActions2.get(maxIndex);
//		if(dir.parent == dir.dir.rotate180()){
//			if(maxIndex==0){
//				dir = stateWithDirections.possibleActions2.get(1);
//				if(dir.parent == dir.dir.rotate180()){
//					dir = stateWithDirections.possibleActions2.get(2);
//				}
//				
//			} else{
//				dir = stateWithDirections.possibleActions2.get(0);
//			}
//		}
		return dir;
	}

	private static void findMaxScore(GameState state, int maxDepth, int[] finalScores) {
		int i = state.source;
		List<Direction> possibleActions;
		int depth = 1;
		currentDepth = depth;
		if (state.isTerminated == false) {

			possibleActions = Action.getPossibleAction(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2], state.pacmanwalls,null);
			GameState[] newStates = getPossibleGameStates(state, possibleActions, depth);
			List<GameState> prevState = Arrays.asList(newStates);
			while (depth <= maxDepth) {
				List<GameState> subStates = new ArrayList<GameState>();
				for (int j=0; j<prevState.size(); j++){
					GameState s = prevState.get(j);
					if (s.isTerminated == false) {
						possibleActions = Action.getPossibleAction(s.pacmanStates[myPacmanIndex][1], s.pacmanStates[myPacmanIndex][2], s.pacmanwalls, null);
						if(isMyPacmanFast(s)){
							subStates.addAll(Arrays.asList(getTwoPossibleGameStates(s, possibleActions, depth)));
							depth++;
						} else{
							subStates.addAll(Arrays.asList(getPossibleGameStates(s, possibleActions, depth)));
						}
					}
				}
				if(subStates.size()==0){
					break;
				}
				prevState = new ArrayList<GameState>(subStates);
				//System.err.println("depth " + depth);
				depth++;
				currentDepth = depth;
			}
			
			int maxScore =(int)(-500 * Math.pow(0.8, depth));
			if (prevState.size() != 0) {
				maxScore = prevState.stream().mapToInt(x -> evaulateGameState(x)).max().getAsInt();
				
				final int maxScoreConst = maxScore;
				Optional<GameState> bestState = prevState.stream().filter(s -> s.score==maxScoreConst).findFirst();
				if(detailedLogging&& bestState.isPresent()){
					System.err.println("Ghost states " + Arrays.deepToString(bestState.get().ghostStates));
					System.err.println("My state " + Arrays.toString(bestState.get().pacmanStates[myPacmanIndex]));
				}
//				for(int j=0; j<prevState.size(); j++){
//					if(j%8==0){
//						extractDistancesFromGameState(prevState.get(j));
//					}
//					
//				}
			}
			finalScores[i] = maxScore;
		} else {
			finalScores[i] = -500;
		}
	}
	
//	public static void extractDistancesFromGameState(GameState state){
//		List<Point> positions = state.positionHistory;
//		for(int i=0; i<positions.size()-1; i++){
//			for(int j=i+1; j<positions.size(); j++){
//				Point p1 = positions.get(i);
//				Point p2 = positions.get(j);
//				
//				PointPair pair1 = new PointPair(p1, p2);
//				PointPair pair2 = new PointPair(p2, p1);
//				int distance = j-i;
//				
//				if(distanceMap.containsKey(pair1)){
//					if(distance < distanceMap.get(pair1)){
//						distanceMap.put(pair1, distance);
//						distanceMap.put(pair2, distance);
//					}
//				} else{
//					distanceMap.put(pair1, distance);
//					distanceMap.put(pair2, distance);
//				}
//				
//				
//			}
//		}
//		
//	}

	private static boolean isMyPacmanFast(GameState state) {
		return state.pacmanStates[0][3]>0;
	}


	
	public static StateWithDirections getTwoPossibleGameStatesWithPossibleActions(GameState state, List<Direction> possibleActions) {

		int[][] successors = getAllPossiblePositionsFromPoint(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2], possibleActions, state.walls.length, state.walls[0].length);
		GameState[] states = generateGameStatesBasedOnMyMove( successors, state, 0);
		List<GameState> lst = new ArrayList<>();
		List<DirectionWithParent> possibleActions2List = new ArrayList<>();
		for(int i=0; i<states.length; i++){
			List<Direction> possibleActions2 = Action.getPossibleAction(states[i].pacmanStates[myPacmanIndex][1], states[i].pacmanStates[myPacmanIndex][2], states[i].pacmanwalls, possibleActions.get(i));
			lst.addAll(Arrays.asList(getPossibleGameStates(states[i], possibleActions2, 0)));
			
			for(Direction dir : possibleActions2){
				possibleActions2List.add(new DirectionWithParent(dir, possibleActions.get(i)));
			}
		}
		GameState[] changedState = lst.toArray(new GameState[lst.size()]);
		
		//changeStateByGhostMove(changedState, 0);

		return new StateWithDirections(changedState, possibleActions, possibleActions2List);
	}
	
	public static class DirectionWithParent{
		Direction dir;
		Direction parent;
		
		public DirectionWithParent(Direction dir, Direction parent) {
			this.dir = dir;
			this.parent = parent;
		}

		@Override
		public String toString() {
			return "DirectionWithParent [dir=" + dir + ", parent=" + parent + "]";
		}	
	}
	
	public static class StateWithDirections{
		
		GameState[] states;
		List<Direction> possibleActions;
		List<DirectionWithParent> possibleActions2;
		
		
		public StateWithDirections(GameState[] states, List<Direction> possibleActions, List<DirectionWithParent> possibleActions2) {
			this.states = states;
			this.possibleActions = possibleActions;
			this.possibleActions2 = possibleActions2;
		}

	}
	
	public static GameState[] getTwoPossibleGameStates(GameState state, List<Direction> possibleActions, int depth) {

		int[][] successors = getAllPossiblePositionsFromPoint(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2], possibleActions, state.walls.length, state.walls[0].length);
		GameState[] states = generateGameStatesBasedOnMyMove(successors, state, depth/2);
		List<GameState> lst = new ArrayList<>();
		for(int i=0; i<states.length; i++){
			List<Direction> possibleActions2 = Action.getPossibleAction(states[i].pacmanStates[myPacmanIndex][1], states[i].pacmanStates[myPacmanIndex][2], states[i].pacmanwalls, possibleActions.get(i));
			lst.addAll(Arrays.asList(getPossibleGameStates(states[i], possibleActions2, depth/2)));
		}
		GameState[] res = lst.toArray(new GameState[lst.size()]);

		return res;
	}

	private static void changeStatesByWorstPossibleGhostMoves(GameState[] states, int depth) {
		Set<Point> lastPacmanPos = new HashSet<>();
		int lastN = 2;
		for(int i=pacmanPositionHistory.size()-1; i>=0 && i>pacmanPositionHistory.size()-1-lastN ; i--){
			lastPacmanPos.add(pacmanPositionHistory.get(i));
		}
		
		for (int i = 0; i < states.length; i++) {
			int pacmanY = states[i].pacmanStates[myPacmanIndex][1];
			int pacmanX = states[i].pacmanStates[myPacmanIndex][2];

			GameState ghostGameState = states[i];

			if (ghostGameState.ghostStates != null) {
				int[][] ghostMoves = new int[ghostGameState.ghostStates.length][2];
				int g = 0;
				for (int[] ghostState : ghostGameState.ghostStates) {
					List<Direction> ghostPossibleActions = Action.getGhostPossibleAction(ghostState[1], ghostState[2], ghostGameState.walls);
					int[][] gSuccessors = getAllPossiblePositionsFromPoint(ghostState[1], ghostState[2], ghostPossibleActions, ghostGameState.walls.length,
							ghostGameState.walls[0].length);
					
					
					int[][] points = getMinAndMaxDistancePoint(pacmanY, pacmanX, gSuccessors, lastPacmanPos);
					int[]minPoint = points[0];
					int[]maxPoint = points[1];
					
					//worst case if we are fast
					if(isMyPacmanFast(ghostGameState) && ghostState[3]>0){
						ghostMoves[g] = maxPoint;
					} else{
						ghostMoves[g] = minPoint;
					}					
					g++;

				}
				

				GameState stateAfterGhostMove = ghostGameState.getNextStateForGhostMoves(ghostMoves, depth);
				states[i] = stateAfterGhostMove;
			}
		}
	}
	
	private static int[][] getMinAndMaxDistancePoint(int pacmanY, int pacmanX, int [][]gSuccessors, Set<Point> lastPacmanPos){
		int minDistance = manhattanDistance(pacmanY, pacmanX, gSuccessors[0][0], gSuccessors[0][1]);
		int[] minPoint = gSuccessors[0];
		int maxDistance = manhattanDistance(pacmanY, pacmanX, gSuccessors[0][0], gSuccessors[0][1]);
		int[] maxPoint = gSuccessors[0];
		for (int j = 1; j < gSuccessors.length; j++) {
			int dist = manhattanDistance(pacmanY, pacmanX, gSuccessors[j][0], gSuccessors[j][1]);
			if(ghostMovementPrint && currentRound == roundToPrint && currentDepth ==ghostDepthToPrint){
				System.err.println(Arrays.toString(gSuccessors[j]) + " " + dist);
				System.err.println(lastPacmanPos);
			}
			//most probably ghosts will follow
			if (minDistance > dist || (minDistance >= dist && lastPacmanPos.contains(new Point(gSuccessors[j][0], gSuccessors[j][1])))) {
				minDistance = dist;
				minPoint = gSuccessors[j];
			} else if(maxDistance < dist){
				maxDistance = dist;
				maxPoint = gSuccessors[j];
			}
		}
		if(ghostMovementPrint && currentRound == roundToPrint && currentDepth ==ghostDepthToPrint){
			System.err.println("minPoint " + Arrays.toString(minPoint));
			System.err.println("minDistance " + minDistance);
		}
		return new int[][]{minPoint, maxPoint};
	}

	public static int evaulateGameState(GameState state) {
		
		int score = state.score;
		
		int pacmanY = state.pacmanStates[myPacmanIndex][1];
		int pacmanX = state.pacmanStates[myPacmanIndex][2];
		
		
		
		List<int[]> foodPositions = state.getFoodPositions();
		
		
		OptionalInt minOp = foodPositions.stream().mapToInt(fpos -> manhattanDistance(pacmanY, pacmanX, fpos[0], fpos[1])).min();
		
		OptionalInt capOp = state.capsules.stream().mapToInt(capPos -> manhattanDistance(pacmanY, pacmanX, capPos.y, capPos.x)).min();
		
		if(score==0){
		if(minOp.isPresent()){
			score-=minOp.getAsInt();
		}
		}
		if(capOp.isPresent()){
			//score-=capOp.getAsInt();
		}
		
		
		return score;
		
	}

	public static GameState[] generateGameStatesBasedOnMyMove(int[][] possiblePos, GameState currenState, int depth) {
		GameState[] gameStates = new GameState[possiblePos.length];
		
		boolean isDeadEnd = false;
		if(possiblePos.length==1){
			isDeadEnd = true;
		}
		
		for (int i = 0; i < possiblePos.length; i++) {
			GameState newState = currenState.getNextStateForMyMove(possiblePos[i][0], possiblePos[i][1], depth, isDeadEnd);
			gameStates[i] = newState;
		}

		return gameStates;

	}
	
	public static GameState[] getPossibleGameStates(GameState state, List<Direction> possibleActions, int depth) {

		int[][] successors = getAllPossiblePositionsFromPoint(state.pacmanStates[myPacmanIndex][1], state.pacmanStates[myPacmanIndex][2], possibleActions, state.walls.length, state.walls[0].length);
		GameState[] states = generateGameStatesBasedOnMyMove(successors, state, depth);

		changeStatesByWorstPossibleGhostMoves(states, depth);

		return states;
	}

	public static int[][] getAllPossiblePositionsFromPoint(int x, int y, List<Direction> possibleActions, int mapY, int mapX) {

		int[][] possiblePos = new int[possibleActions.size()][2];

		for (int i = 0; i < possibleActions.size(); i++) {
			possiblePos[i][0] += Math.floorMod(x + possibleActions.get(i).dx, mapY);
			possiblePos[i][1] += Math.floorMod(y + possibleActions.get(i).dy, mapX);
		}

		return possiblePos;

	}
	
}

class GameState {
	
	static final int SPEEDBOOST = 21;
	static double MULTIPLIER = 0.8;
	

	int source;
	
	final boolean[][] food;
	final boolean[][] walls;
	final boolean[][] pacmanwalls;
	List<Point> capsules;
	final int score;
	final int tick;

	// id,y,x,fast
	final int[][] pacmanStates;
	// id,y,x,scared,frozen,dirx,diry
	final int[][] ghostStates;
	final boolean isTerminated;
	
	List<Point> positionHistory;
	
	final int myPacmanIndex;

	public GameState(int mypacmanindex, boolean[][] food, boolean[][] walls, boolean[][] pacmanwalls, List<Point> capsules, int score, int tick, int[][] pacmanStates,
			int[][] ghostStates, boolean isTerminated, List<Point> positionHistory) {
		this.food = food;
		this.walls = walls;
		this.pacmanwalls = pacmanwalls;
		this.capsules = capsules;
		this.score = score;
		this.tick = tick;
		this.pacmanStates = pacmanStates;
		this.ghostStates = ghostStates;
		this.isTerminated = isTerminated;
		this.positionHistory = positionHistory;
		this.myPacmanIndex = mypacmanindex;
	}

	public GameState(int mypacmanindex,int tick, int height, int width, int np, int ng, char[][] fields, String[][] pacmans, String[][] ghosts) {
		this.source = 0;
		this.isTerminated = false;
		this.tick = tick;
		this.myPacmanIndex = mypacmanindex;

		capsules = new ArrayList<>();
		positionHistory  = new ArrayList<>();

		food = new boolean[height][width];
		walls = new boolean[height][width];
		pacmanwalls = new boolean[height][width];

		pacmanStates = new int[np][4];
		ghostStates = new int[ng][7];

		score = 0;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				char layoutChar = fields[y][x];
				if (layoutChar == 'F') {
					walls[y][x] = true;
					pacmanwalls[y][x] = true;
				} else if (layoutChar == '1') {
					food[y][x] = true;
				} else if (layoutChar == '+') {
					capsules.add(new Point(y, x));
				} else if (layoutChar == ' ') {

				} else if (layoutChar == 'G') {
					pacmanwalls[y][x] = true;
				}

			}
		}

		int maxId = 0;
		for (int i = 0; i < pacmans.length; i++) {

			int id = Integer.parseInt(pacmans[i][0]);
			if (id > maxId) {
				maxId = id;
			}
			int y = Integer.parseInt(pacmans[i][2]);
			int x = Integer.parseInt(pacmans[i][3]);
			int f = Integer.parseInt(pacmans[i][4]);

			pacmanStates[i] = new int[] { id, y, x, f };
		}

		for (int i = 0; i < ghosts.length; i++) {

			int y = Integer.parseInt(ghosts[i][1]);
			int x = Integer.parseInt(ghosts[i][2]);
			int e = Integer.parseInt(ghosts[i][3]);
			int f = Integer.parseInt(ghosts[i][4]);

			ghostStates[i] = new int[] { maxId++, y, x, e, f };
		}

	}
	
	public List<int[]> getFoodPositions(){
		List<int[]> list = new ArrayList<int[]>();
		
		for(int i=0; i<food.length; i++){
			for(int j=0; j<food[0].length; j++){
				if(food[i][j]){
					list.add(new int[]{i,j});
				}
				
			}
		}
		
		return list;
	}

	public static boolean[][] clone(boolean[][] a) {
		if (a != null && a.length > 0) {
			boolean[][] b = new boolean[a.length][a[0].length];
			for (int i = 0; i < a.length; i++) {
				System.arraycopy(a[i], 0, b[i], 0, a[0].length);
			}
			return b;
		}
		return null;
	}

	public static int[][] clone(int[][] a) {
		if (a != null && a.length > 0) {
			int[][] b = new int[a.length][a[0].length];
			for (int i = 0; i < a.length; i++) {
				System.arraycopy(a[i], 0, b[i], 0, a[0].length);
			}
			return b;
		}
		return null;
	}

	public GameState getNextStateForMyMove(int y, int x, int depth, boolean isDeadEnd) {

		int newScore = this.score;

		
		double scoreMultiplier = Math.pow(MULTIPLIER, depth);
		boolean isTerminated = this.isTerminated;

		int[][] newpacmanStates = clone(this.pacmanStates);
		newpacmanStates[myPacmanIndex][1] = y;
		newpacmanStates[myPacmanIndex][2] = x;
		
		List<Point> newPositionHistory = new ArrayList<>(this.positionHistory);
		newPositionHistory.add(new Point(y,x));
		

		boolean[][] newFood = clone(this.food);

		List<Point> newCapsules = new ArrayList<>(this.capsules);

		int[] myState = newpacmanStates[0];
		if (newFood[myState[1]][myState[2]]) {
			newFood[myState[1]][myState[2]] = false;
			newScore +=  (10 * scoreMultiplier);
		}

		boolean capsuleCollected = false;
		for (int i = 0; i < capsules.size(); i++) {
			if (myState[1] == capsules.get(i).x && myState[2] == capsules.get(i).y) {
				if(myState[3]==0){
					newScore += (50 * scoreMultiplier);
				}
				newCapsules.remove(i);
				capsuleCollected = true;
				newpacmanStates[0][3] = SPEEDBOOST;
				myState[3] = SPEEDBOOST;
			}

		}

		if (ghostStates != null) {
			int[][] newGhostStates = clone(ghostStates);

			for(int i=0; i< ghostStates.length; i++){
				if(capsuleCollected){
					newGhostStates[i][3]=SPEEDBOOST;
				}

				if (newGhostStates[i][1] == myState[1] && 
						newGhostStates[i][2] == myState[2] && 
						(myState[3] == 0 || (myState[3] > 0 && newGhostStates[i][3] == 0))) {
					newScore -= 500 * scoreMultiplier;
					isTerminated = true;
				} else if(newGhostStates[i][1] == myState[1] && newGhostStates[i][2] == myState[2] && myState[3] > 0 && newGhostStates[i][3]>0 ){
					
					newScore+= (100 * scoreMultiplier);
					newGhostStates[i][3] = 0;
				}
				
				
				
			}
			if(newpacmanStates[0][3]>0){
				newpacmanStates[0][3]-=1;
			}

			return new GameState(myPacmanIndex, newFood, walls, pacmanwalls, newCapsules, newScore, tick, newpacmanStates, newGhostStates, isTerminated, newPositionHistory);
		} else {
			return new GameState(myPacmanIndex, newFood, walls, pacmanwalls, newCapsules, newScore, tick, newpacmanStates, null, isTerminated, newPositionHistory);
		}

	}

	public GameState getNextStateForGhostMoves(int[][] moves, int depth) {
		
		double scoreMultiplier = Math.pow(MULTIPLIER, depth);

		int newScore = this.score;
		boolean isTerminated = false;

		int[][] newpacmanStates = clone(this.pacmanStates);
		int[] myState = newpacmanStates[0];
		boolean[][] newFood = clone(this.food);
		List<Point> newCapsules = new ArrayList<>(this.capsules);

		int[][] newGhostStates = clone(ghostStates);

		for (int i = 0; i < ghostStates.length; i++) {

			newGhostStates[i][1] = moves[i][0];
			newGhostStates[i][2] = moves[i][1];

			if (newGhostStates[i][1] == myState[1] && newGhostStates[i][2] == myState[2] && (myState[3] == 0 || (myState[3] > 0 && newGhostStates[i][1] == 0))) {
				newScore -= 500 * scoreMultiplier;
				isTerminated = true;

			}
			
			if(newGhostStates[i][3]>0){
				newGhostStates[i][3]-=1;
			}
		}

		return new GameState(myPacmanIndex,newFood, walls, pacmanwalls, newCapsules, newScore, tick + 1, newpacmanStates, newGhostStates, isTerminated, positionHistory);

	}

}

class Action {

	static Map<Point, List<Direction>> cache = new HashMap<Point, List<Direction>>();
	static Map<Point, List<Direction>> ghost_cache = new HashMap<Point, List<Direction>>();

	public static List<Direction> getPossibleAction(int x, int y, boolean[][] walls, Direction lastDir) {
		List<Direction> possible = new ArrayList<Direction>();
		Point pos = new Point(x, y);
		if (cache.containsKey(pos)) {
			return cache.get(pos);
		}

		
		for (Direction dir : Direction.values()) {
			int next_x = x + dir.dx;
			int next_y = y + dir.dy;
			int walls_width = walls[0].length;
			int walls_height = walls.length;
			if (!walls[Math.floorMod(next_x, walls_height)][Math.floorMod(next_y, walls_width)]) {
				possible.add(dir);
			}
		}
		cache.put(pos, new ArrayList<>(possible));
		
		if(lastDir!= null && possible.size()>1 && possible.contains(lastDir.rotate180())){
			possible.remove(lastDir.rotate180());
		}
		
		return possible;
	}

	public static List<Direction> getGhostPossibleAction(int x, int y, boolean[][] ghostwalls) {
		List<Direction> possible = new ArrayList<Direction>();
		Point pos = new Point(x, y);
		if (ghost_cache.containsKey(pos)) {
			return ghost_cache.get(pos);
		}

		for (Direction dir : Direction.values()) {
			int next_x = x + dir.dx;
			int next_y = y + dir.dy;
			int walls_width = ghostwalls[0].length;
			int walls_height = ghostwalls.length;
			if (!ghostwalls[Math.floorMod(next_x, walls_height)][Math.floorMod(next_y, walls_width)]) {
				possible.add(dir);
			}
		}

		ghost_cache.put(pos, possible);
		return possible;
	}

}

enum Direction {

	// use magic numbers to set the ordinal (used for rotation),
	// and the dx and dy of each direction.
	NORTH(0, -1, 0, '^'), EAST(1, 0, 1, '>'), SOUTH(2, 1, 0, 'v'), WEST(3, 0, -1, '<');

	private final int r180index;
	public final int dx, dy;
	public final char dir;

	private Direction(int ordinal, int dx, int dy, char dir) {
		// from the ordinal, dx, and dy, we can calculate all the other
		// constants.
		this.dx = dx;
		this.dy = dy;
		this.r180index = (ordinal + 2) % 4;
		this.dir = dir;
	}

	// Rotate 180 degrees
	public Direction rotate180() {
		return values()[r180index];
	}

}

class Reader {
	int[] data;
	char[][] fields;
	String[][] pacmans;
	String[][] ghosts;

	int X;
	int Y;
	int pacmanCount;
	int ghostCount;

	public Reader(Scanner scanner) {
		data = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
		Scanner secondLine = new Scanner(scanner.nextLine());

		int tick = data[1];

		X = secondLine.nextInt();
		Y = secondLine.nextInt();
		pacmanCount = secondLine.nextInt();
		ghostCount = secondLine.nextInt();
		if (secondLine.hasNextLine())
			System.err.println("\nGot message:" + secondLine.nextLine() + "\n");

		fields = Stream.generate(() -> scanner.nextLine().substring(0, Y).toCharArray()).limit(X).toArray(char[][]::new);

		pacmans = Stream.generate(() -> scanner.nextLine().split(" ")).limit(pacmanCount).toArray(String[][]::new);

		ghosts = Stream.generate(() -> scanner.nextLine().split(" ")).limit(ghostCount).toArray(String[][]::new);

	}

	public GameState getGameState() {
		return new GameState(data[2],data[1], X, Y, pacmanCount, ghostCount, fields, pacmans, ghosts);
	}
}

class PointPair{
	Point point1;
	Point point2;
	
	public PointPair(Point point1, Point point2){
		this.point1 = point1;
		this.point2 = point2;
		
	}
}

class ElapsedTimer {
	private long timeStarted = 0;

	public void start() {
		timeStarted = System.nanoTime();
	}

	public long getElapsedTime() {
		if (timeStarted == 0) {
			return 0;
		}

		long elapsed = System.nanoTime() - timeStarted;
		return TimeUnit.NANOSECONDS.toMillis(elapsed);
	}
}
